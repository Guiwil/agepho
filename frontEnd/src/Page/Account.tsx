import '../App.css';
import { Button, Tooltip, Container, Paper, Avatar, Typography, Grid, CardHeader, Card, CardActions, CardContent, CardMedia, Collapse, IconButton, IconButtonProps } from '@material-ui/core';
import { TextField } from '@material-ui/core';
import { PATHS } from '../routes';
import { Form, Formik, FormikProps } from 'formik';
import * as Yup from 'yup';
import { makeStyles, styled } from '@material-ui/core/styles';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import AuthenticationService from '../auth';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import React from 'react';
import NavBar from './NavBar';
import { EditAccount } from '../Composant/EditAccount';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(10),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    logo: {
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    titre: {
        marginTop: theme.spacing(8),
        color: "black",
    },
    container: {
        marginTop: theme.spacing(5),
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: "#F8A055",
    },
    error: {
        backgroundColor: "red",
        color: "white",
        textAlign: "center"
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(3),
    },
    fond: {
        backgroundColor: "white",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",

    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    createAccount: {
        marginRight: theme.spacing(3),
        float: "right"
    },
    img: {
        backgroundImage: `url("https://via.placeholder.com/500")`,
        clipPath: "ellipse(33% 50%)",
        backgroundSize: "cover",
        backgroundRepeat: "no-repeat",
    }
}));

interface ExpandMoreProps extends IconButtonProps {
    expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
})(({ theme }) => ({
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
    }),
}));


interface ConnexionForm {
    email: string
    password: string
}


const Account = () => {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);
    const [open, setOpen] = React.useState(false);

    const closeModal = () => {
        setOpen(false)
    }

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };
    return (
        <div style={{ minHeight: '100vh', maxHeight: "500vh" }} className={classes.fond}>
            <NavBar />
            <br />
            <Grid container justifyContent='center' alignItems='center' alignContent='center' style={{ height: '20vh' }}>
                <Grid item xs={2}></Grid>
                <Grid item xs={3}>
                    <h1> Mon compte</h1>
                </Grid>
                <Grid item xs={1}></Grid>
                <Grid item xs={1}>
                    <Button onClick={() => { setOpen(true) }}>Edit</Button>
                </Grid>
            </Grid>
            <Grid container justifyContent='center' alignItems='center' alignContent='center' >
                <Grid item xs={6} style={{ height: '30vh' }}>
                    <h4>Prénom : {AuthenticationService.firstName}</h4>
                    <h4>Nom : {AuthenticationService.nom}</h4>
                </Grid>
            </Grid>
            <EditAccount open={open} closeModal={closeModal} />
        </div>


    );
}

export default Account;
