import '../App.css';
import { Button, Tooltip, Container, Paper, Avatar, Typography, Grid, CardHeader, Card, CardActions, CardContent, CardMedia, Collapse, IconButton, IconButtonProps, Divider } from '@material-ui/core';
import { TextField } from '@material-ui/core';
import { PATHS } from '../routes';
import { Form, Formik, FormikProps } from 'formik';
import * as Yup from 'yup';
import { makeStyles, styled } from '@material-ui/core/styles';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import AuthenticationService from '../auth';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import React, { useEffect, useState } from 'react';
import NavBar from './NavBar'
import axios from 'axios';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(10),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    logo: {
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    titre: {
        marginTop: theme.spacing(1)
    },
    sousTitre: {
        marginBottom: theme.spacing(3)
    },
    container: {
        marginTop: theme.spacing(5),
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: "#F8A055",
    },
    error: {
        backgroundColor: "red",
        color: "white",
        textAlign: "center"
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(3),
    },
    fond: {
        backgroundColor: "white",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",

    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    createAccount: {
        marginRight: theme.spacing(3),
        float: "right"
    }
}));

interface ExpandMoreProps extends IconButtonProps {
    expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
})(({ theme }) => ({
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
    }),
}));

export const EspaceCard = (props: any) => {
    const classes = useStyles()
    const [expanded, setExpanded] = React.useState(false);
    const [compteur, setCompteur] = useState(props.pub.image.like)
    const [userLike, setUserLike] = useState(AuthenticationService.like)

    useEffect(() => {
        console.log(AuthenticationService.like)
        console.log(userLike)
        console.log(props.pub.image.id)
        console.log(userLike.indexOf(props.pub.image.id))
    }, [])

    const handleExpandClick = () => {
        setExpanded(!expanded);
    };

    const like = (pub: any, index: number) => {
        console.log(pub.image.id)
        axios.post('http://127.0.0.1:6868/like', { positionEmail: pub.positionEmail, position: pub.position, "imageId": pub.image.id, "email": AuthenticationService.email }).then((data) => {
            console.log("bbbbbb")

            if (data.data !== "Error") {
                setCompteur(compteur + 1)
                console.log(compteur)
                AuthenticationService.updateLike(data.data.like).then(() => {
                    setUserLike(data.data.like)
                })

            }
        })
    }

    return (
        <Grid item xs={9} md={5} lg={3} style={{ width: "100%", margin: "0 auto" }}>
            <Card >
                <CardHeader
                    avatar={
                        <Avatar aria-label="recipe">
                            {props.pub.user.name[0]}
                        </Avatar>
                    }
                    title={props.pub.user.firstName}
                    subheader={props.pub.user.email}
                />
                <CardMedia
                    component="img"
                    height="194"
                    image={props.pub.image.image}
                    alt="Image publication"
                />
                <CardContent>
                    <Typography variant="h6" >
                        {props.pub.image.titre}
                    </Typography>
                    <Typography variant="subtitle2" className={classes.sousTitre}>
                        {props.pub.image.date}
                    </Typography>
                    <Divider></Divider>
                    <Typography variant="body2" className={classes.titre}>
                        {props.pub.image.description}
                    </Typography>
                </CardContent>
                <CardActions disableSpacing>
                    {
                        userLike.indexOf(props.pub.image.id) !== -1 &&
                        <IconButton aria-label="add to favorites" disabled>
                            <ThumbUpAltIcon />
                        </IconButton>
                    }
                    {
                        userLike.indexOf(props.pub.image.id) === -1 &&
                        <IconButton aria-label="add to favorites" onClick={() => { like(props.pub, props.indexe) }}>
                            <ThumbUpAltIcon />
                        </IconButton>
                    }

                    <IconButton aria-label="share">
                        {compteur}
                    </IconButton>

                </CardActions>
            </Card>
        </Grid>
    )
}