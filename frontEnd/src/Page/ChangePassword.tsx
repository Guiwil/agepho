import '../App.css';
import { Button, Tooltip, Container, Paper, Avatar, Typography, Grid, CardHeader, Card, CardActions, CardContent, CardMedia, Collapse, IconButton, IconButtonProps } from '@material-ui/core';
import { TextField } from '@material-ui/core';
import { PATHS } from '../routes';
import { Form, Formik, FormikProps } from 'formik';
import * as Yup from 'yup';
import { makeStyles, styled } from '@material-ui/core/styles';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import AuthenticationService from '../auth';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import React from 'react';
import NavBar from './NavBar';
import { EditAccount } from '../Composant/EditAccount';
import { useSnackbar } from 'notistack';
import { useHistory } from 'react-router-dom';
import axios from 'axios';
import * as sha512 from 'js-sha512';


const useStyles = makeStyles((theme) => ({
    addUser: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
    editJ_button: {
        marginRight: theme.spacing(2),
    },
    AjoutPForm: {
        marginBottom: theme.spacing(3),
    },
    AjoutPForm_fin: {
        marginBottom: theme.spacing(2),
        textAlign: "center",
    },
    AjoutPForm_button: {
        marginRight: theme.spacing(2),
    },
    fond: {
        backgroundColor: "black",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",

    },
    save: {
        marginRight: theme.spacing(4),
    },
    ucesoOpenList: {
        marginBottom: theme.spacing(0.5),
    },
    spaceBetweenUceso: {
        marginBottom: theme.spacing(1),
    },
    error: {
        backgroundColor: "red",
        color: "white",
        textAlign: "center",
    },
}));


interface ConnexionForm {
    email: string
    password: string
}

const ValidEmail = () => {
    const classes = useStyles();
    const history = useHistory()

    const [open, setOpen] = React.useState(false);
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    return (
        <div style={{ minHeight: '100vh', maxHeight: "500vh" }} className={classes.fond}>
            <Grid
                container
                alignItems="center"
                alignContent="center"
                justifyContent="center"
            >
                <Grid item xs={12} style={{ height: "15vh" }}></Grid>
                <Grid item xs={12} style={{ height: "15vh" }}><h1 style={{ color: "white" }}>Mot de passe oublié</h1></Grid>
                <Card>

                    <Formik
                        initialValues={{ email: "", code: "", newPassword: "", checkNewPassword: "" }}
                        onSubmit={(values, { resetForm }) => {
                            let infoKey = enqueueSnackbar("Ajout en cours", {
                                variant: "info",
                                autoHideDuration: 5000,
                            });
                            var newPass = sha512.sha512(values.newPassword);

                            axios.post('http://127.0.0.1:6868/changePassordCode', { "email": values.email, "code": values.code, "password": newPass }).then((data) => {
                                closeSnackbar(infoKey)
                                if (data.data !== "OK") {
                                    enqueueSnackbar("Un problème est survenu", {
                                        variant: "error",
                                        autoHideDuration: 3000,
                                    });
                                }
                                else {
                                    enqueueSnackbar("Email envoyé", {
                                        variant: "success",
                                        autoHideDuration: 3000,
                                    });
                                    history.push('/')
                                }

                            })
                            resetForm();
                        }}
                        validationSchema={Yup.object().shape({
                            email: Yup.string()
                                .required("L'email est manquant"),
                            code: Yup.string()
                                .required("Le code est manquant"),
                            newPassword: Yup.string()
                                .matches(
                                    /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*()/]).{8,20}\S$/, "Merci de mettre au moins une majuscule, un nombre, un caractère spéciale"
                                )
                                .required(
                                    'Entrez un mot de passe valide'
                                ),
                            checkNewPassword: Yup.string()
                                .oneOf([Yup.ref('newPassword'), null], 'Mot de passe différent')
                        })}
                    >
                        {({ values, errors, touched, handleChange, handleBlur }) => (
                            <Form>
                                <Grid
                                    container
                                    alignItems="center"
                                    alignContent="center"
                                    justifyContent="center"
                                >
                                    <Grid item xs={8}>
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="email"
                                            value={values.email}
                                            label="Adresse email"
                                            name="email"
                                            autoComplete="email"
                                            autoFocus
                                            type="email"
                                            helperText={
                                                errors.email && touched.email
                                                    ? errors.email
                                                    : ''
                                            }
                                            error={
                                                errors.email && touched.email
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                    <Grid item xs={8}>
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="code"
                                            value={values.code}
                                            label="Code"
                                            name="code"
                                            autoComplete="code"
                                            helperText={
                                                errors.code && touched.code
                                                    ? errors.code
                                                    : ''
                                            }
                                            error={
                                                errors.code && touched.code
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={5} className={classes.AjoutPForm_button}>
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="newPassword"
                                            value={values.newPassword}
                                            label="Nouveau mot de passe"
                                            name="newPassword"
                                            type="password"
                                            helperText={
                                                errors.newPassword && touched.newPassword
                                                    ? errors.newPassword
                                                    : ''
                                            }
                                            error={
                                                errors.newPassword && touched.newPassword
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={5} >
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="checkNewPassword"
                                            value={values.checkNewPassword}
                                            label="Validation mot de passe"
                                            name="checkNewPassword"
                                            type="password"
                                            helperText={
                                                errors.checkNewPassword && touched.checkNewPassword
                                                    ? errors.checkNewPassword
                                                    : ''
                                            }
                                            error={
                                                errors.checkNewPassword && touched.checkNewPassword
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid
                                    container
                                    alignItems="center"
                                    alignContent="center"
                                    justifyContent="center"
                                >
                                    <Grid item xs={6} md={4} className={classes.addUser}>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            size="small"
                                            type="submit"
                                        >
                                            Valider
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Form>
                        )}
                    </Formik>
                </Card>

            </Grid>
        </div>


    );
}

export default ValidEmail;
