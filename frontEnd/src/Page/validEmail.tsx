import '../App.css';
import { Button, Tooltip, Container, Paper, Avatar, Typography, Grid, CardHeader, Card, CardActions, CardContent, CardMedia, Collapse, IconButton, IconButtonProps } from '@material-ui/core';
import { TextField } from '@material-ui/core';
import { PATHS } from '../routes';
import { Form, Formik, FormikProps } from 'formik';
import * as Yup from 'yup';
import { makeStyles, styled } from '@material-ui/core/styles';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import AuthenticationService from '../auth';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import React from 'react';
import NavBar from './NavBar';
import { EditAccount } from '../Composant/EditAccount';
import { useSnackbar } from 'notistack';
import { useHistory } from 'react-router-dom';
import axios from 'axios';


const useStyles = makeStyles((theme) => ({
    addUser: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
    editJ_button: {
        marginRight: theme.spacing(2),
    },
    AjoutPForm: {
        marginBottom: theme.spacing(3),
    },
    AjoutPForm_fin: {
        marginBottom: theme.spacing(2),
        textAlign: "center",
    },
    AjoutPForm_button: {
        marginRight: theme.spacing(2),
    },
    save: {
        marginRight: theme.spacing(4),
    },
    ucesoOpenList: {
        marginBottom: theme.spacing(0.5),
    },
    spaceBetweenUceso: {
        marginBottom: theme.spacing(1),
    },
    error: {
        backgroundColor: "red",
        color: "white",
        textAlign: "center",
    },
    fond: {
        backgroundColor: "black",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",

    },
}));


interface ConnexionForm {
    email: string
    password: string
}

const ValidEmail = () => {
    const classes = useStyles();
    const history = useHistory()

    const [open, setOpen] = React.useState(false);
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    return (
        <div style={{ minHeight: '100vh', maxHeight: "500vh" }} className={classes.fond}>
            <Grid
                container
                alignItems="center"
                alignContent="center"
                justifyContent="center"
            >
                <Grid item xs={12} style={{ height: "15vh" }}></Grid>
                <Grid item xs={12} style={{ height: "15vh" }}><h1 style={{ color: "white" }}>Validation email</h1></Grid>
                <Card>

                    <Formik
                        initialValues={{ email: "", code: "" }}
                        onSubmit={(values, { resetForm }) => {
                            let infoKey = enqueueSnackbar("Validation en cours", {
                                variant: "info",
                                autoHideDuration: 5000,
                            });

                            axios.post('http://127.0.0.1:6868/confirmUser', { "email": values.email, "code": values.code }).then((data) => {
                                closeSnackbar(infoKey)
                                if (data.data !== "OK") {
                                    enqueueSnackbar("Un problème est survenu", {
                                        variant: "error",
                                        autoHideDuration: 3000,
                                    });
                                }
                                else {
                                    enqueueSnackbar("Email validé", {
                                        variant: "success",
                                        autoHideDuration: 3000,
                                    });
                                    history.push("/")
                                }

                            })
                            resetForm();
                        }}
                        validationSchema={Yup.object().shape({
                            email: Yup.string()
                                .required("Le titre est manquant"),
                            code: Yup.string()
                                .required(
                                    'Entrez votre code'
                                )
                        })}
                    >
                        {({ values, errors, touched, handleChange, handleBlur }) => (
                            <Form>
                                <Grid
                                    container
                                    alignItems="center"
                                    alignContent="center"
                                    justifyContent="center"
                                >
                                    <Grid item xs={12} sm={8}>
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="email"
                                            value={values.email}
                                            label="Adresse email"
                                            name="email"
                                            autoComplete="email"
                                            autoFocus
                                            type="email"
                                            helperText={
                                                errors.email && touched.email
                                                    ? errors.email
                                                    : ''
                                            }
                                            error={
                                                errors.email && touched.email
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={5} className={classes.save}>
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="code"
                                            value={values.code}
                                            label="Code"
                                            name="code"
                                            autoComplete="code"
                                            helperText={
                                                errors.code && touched.code
                                                    ? errors.code
                                                    : ''
                                            }
                                            error={
                                                errors.code && touched.code
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid
                                    container
                                    alignItems="center"
                                    alignContent="center"
                                    justifyContent="center"
                                >
                                    <Grid item xs={6} md={3} className={classes.addUser}>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            size="small"
                                            type="submit"
                                        >
                                            Valider
                                        </Button>
                                    </Grid>
                                    <Grid
                                        item
                                        xs={6}
                                        md={3}
                                        className={classes.addUser}
                                        style={{ textAlign: "center" }}
                                    >
                                        <Button
                                            size="small"
                                            variant="contained"
                                            onClick={() => history.push("/")}
                                            color="secondary"
                                        >
                                            Fermer
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Form>
                        )}
                    </Formik>
                </Card>

            </Grid>
        </div>


    );
}

export default ValidEmail;
