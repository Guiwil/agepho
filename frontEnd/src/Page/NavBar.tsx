import { AppBar, Container, Toolbar, Box, IconButton, Menu, MenuItem, Button, Tooltip, Avatar, Typography } from "@material-ui/core";
import React from "react";
import { useHistory } from "react-router-dom";
import AuthenticationService from "../auth";
import { PATHS } from "../routes";
import logo from './../img/logo.png';
import { makeStyles } from '@material-ui/core/styles';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(10),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    logo: {
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    titre: {
        marginTop: theme.spacing(8),
        color: "black",
    },
    container: {
        marginTop: theme.spacing(5),
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: "#F8A055",
    },
    error: {
        backgroundColor: "red",
        color: "white",
        textAlign: "center"
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(3),
    },
    fond: {
        backgroundImage: `url(${logo})`,
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",
        backgroundPosition: "center",


    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    createAccount: {
        marginRight: theme.spacing(3),
        float: "right"
    }
}));

const pages = ['Home'];
const settings = ['Mon compte', 'Logout'];

const NavBar = () => {
    const history = useHistory();
    const classes = useStyles()
    const [anchorElNav, setAnchorElNav] = React.useState<null | HTMLElement>(null);
    const [anchorElUser, setAnchorElUser] = React.useState<null | HTMLElement>(null);

    const handleOpenNavMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElNav(event.currentTarget);
    };
    const handleOpenUserMenu = (event: React.MouseEvent<HTMLElement>) => {
        setAnchorElUser(event.currentTarget);
    };

    const handleCloseNavMenu = () => {
        setAnchorElNav(null);
    };

    const handleCloseUserMenu = () => {
        setAnchorElUser(null);
    };

    return (
        <AppBar position="static">
            <Container maxWidth="xl">
                <Toolbar disableGutters>
                    <img src={logo} style={{ height: 30 }}></img>
                    <Box sx={{ flexGrow: 1 }}>
                        {pages.map((page) => (
                            <Button
                                key={page}
                                onClick={() => {
                                    console.log(page)
                                    if (page === "Home") {
                                        history.push(PATHS.ESPACE)
                                    }
                                    else {
                                        handleCloseNavMenu()
                                    }
                                }}
                                style={{ color: "white" }}

                            >
                                {page}
                            </Button>
                        ))}
                    </Box>

                    <Box >
                        <Tooltip title="Open settings">
                            <IconButton onClick={handleOpenUserMenu} >
                                <Avatar alt="Remy Sharp" >
                                    {AuthenticationService.firstName[0]}
                                </Avatar>
                            </IconButton>
                        </Tooltip>
                        <Menu
                            style={{ marginTop: "45px" }}
                            id="menu-appbar"
                            anchorEl={anchorElUser}
                            anchorOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            keepMounted
                            transformOrigin={{
                                vertical: 'top',
                                horizontal: 'right',
                            }}
                            open={Boolean(anchorElUser)}
                            onClose={handleCloseUserMenu}
                        >
                            {settings.map((setting) => (
                                <MenuItem key={setting} onClick={handleCloseUserMenu}>
                                    <Typography style={{ textAlign: "center" }} onClick={() => {
                                        if (setting === "Mon compte") {
                                            history.push(PATHS.PUBLICATION)
                                        }
                                        else {
                                            AuthenticationService.logout().then(() => {
                                                history.push("/")
                                            })
                                        }
                                    }}>{setting}</Typography>
                                </MenuItem>
                            ))}
                        </Menu>
                    </Box>
                </Toolbar>
            </Container>
        </AppBar>
    );
};
export default NavBar;