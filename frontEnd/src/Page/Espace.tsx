import '../App.css';
import { Button, Tooltip, Container, Paper, Avatar, Typography, Grid, CardHeader, Card, CardActions, CardContent, CardMedia, Collapse, IconButton, IconButtonProps } from '@material-ui/core';
import { TextField } from '@material-ui/core';
import { PATHS } from '../routes';
import { Form, Formik, FormikProps } from 'formik';
import * as Yup from 'yup';
import { makeStyles, styled } from '@material-ui/core/styles';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import AuthenticationService from '../auth';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import React, { useEffect } from 'react';
import NavBar from './NavBar'
import axios from 'axios';
import { EspaceCard } from './EspaceCard';
import CachedIcon from '@material-ui/icons/Cached';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(10),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    logo: {
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    titre: {
        marginTop: theme.spacing(8),
        color: "black",
    },
    container: {
        marginTop: theme.spacing(5),
    },
    refresh: {
        marginBottom: theme.spacing(4),
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: "#F8A055",
    },
    error: {
        backgroundColor: "red",
        color: "white",
        textAlign: "center"
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(3),
    },
    fond: {
        backgroundColor: "white",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",

    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    createAccount: {
        marginRight: theme.spacing(3),
        float: "right"
    }
}));




interface ConnexionForm {
    email: string
    password: string
}


const Espace = () => {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);
    const [publications, setPublications] = React.useState(AuthenticationService.images as any);
    const [load, setLoad] = React.useState(false);

    useEffect(() => {
        axios.get('http://127.0.0.1:6868/randomPublication?email=' + AuthenticationService.email).then((data) => {
            console.log("bbbbbb")

            if (data.data !== "Utilisateur non trouve") {
                console.log(data.data)
                setPublications(data.data)
                setLoad(true)
            }
        })
    }, [])

    const update = () => {
        axios.get('http://127.0.0.1:6868/randomPublication?email=' + AuthenticationService.email).then((data) => {
            if (data.data !== "Utilisateur non trouve") {
                console.log(data.data)
                setPublications(data.data)
                setLoad(true)
            }
        })
    }

    return (
        <div style={{ minHeight: '100vh', maxHeight: "500vh" }} className={classes.fond}>
            <NavBar />
            <br />
            <Grid container justifyContent='center' alignItems='center' alignContent='center' style={{ height: '15vh' }}>
                <Grid item xs={12}>
                    <h1>Accueil</h1>
                </Grid>
                <Grid item xs={12} className={classes.refresh}>
                    <Button variant="contained" className={classes.refresh} onClick={() => update()}><CachedIcon /></Button>
                </Grid>
            </Grid>
            <Grid container justifyContent='center' alignItems='center' alignContent='center' style={{ minHeight: '60vh' }}>
                {load &&
                    publications.map((pub: any, indexe: number) => {
                        return (
                            <EspaceCard pub={pub} index={indexe} />
                        )
                    })}
                {!load &&
                    <Grid container justifyContent='center' alignItems='center' alignContent='center'>
                        <Grid item xs={12}>
                            Chargement en cours
                        </Grid>
                    </Grid>
                }
            </Grid >
        </div >


    );
}

export default Espace;
