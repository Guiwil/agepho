import '../App.css';
import { Button, Tooltip, Container, Paper, Avatar, Typography, Grid, CardHeader, Card, CardActions, CardContent, CardMedia, Collapse, IconButton, IconButtonProps } from '@material-ui/core';
import { TextField } from '@material-ui/core';
import { PATHS } from '../routes';
import { Form, Formik, FormikProps } from 'formik';
import * as Yup from 'yup';
import { makeStyles, styled } from '@material-ui/core/styles';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import AuthenticationService from '../auth';
import ThumbUpAltIcon from '@material-ui/icons/ThumbUpAlt';
import React from 'react';
import NavBar from './NavBar'
import { AddPublication } from '../Composant/AddPublication';
import { EditPublication } from '../Composant/EditPublication';
import { EditAccount } from '../Composant/EditAccount';
import AssignmentIndIcon from '@material-ui/icons/AssignmentInd';
import EditIcon from '@material-ui/icons/Edit';
import DeleteIcon from '@material-ui/icons/Delete';
import { DeletePublication } from '../Composant/DeletePublication';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(10),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    logo: {
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    titre: {
        marginTop: theme.spacing(8),
        color: "black",
    },
    container: {
        marginTop: theme.spacing(5),
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: "#F8A055",
    },
    error: {
        backgroundColor: "red",
        color: "white",
        textAlign: "center"
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(3),
    },
    fond: {
        backgroundColor: "white",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",

    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    createAccount: {
        marginRight: theme.spacing(3),
        float: "right"
    }
}));

interface ExpandMoreProps extends IconButtonProps {
    expand: boolean;
}

const ExpandMore = styled((props: ExpandMoreProps) => {
    const { expand, ...other } = props;
    return <IconButton {...other} />;
})(({ theme }) => ({
    marginLeft: 'auto',
    transition: theme.transitions.create('transform', {
        duration: theme.transitions.duration.shortest,
    }),
}));


interface ConnexionForm {
    email: string
    password: string
}


const Publication = () => {
    const classes = useStyles();
    const [expanded, setExpanded] = React.useState(false);
    const [open, setOpen] = React.useState(false);
    const [openEdit, setOpenEdit] = React.useState(false);
    const [openEditPublication, setOpenEditPublication] = React.useState(false);
    const [openDeletePublication, setOpenDeletePublication] = React.useState(false);

    const [publications, setPublications] = React.useState(AuthenticationService.images);

    const closeModal = () => {
        setOpen(false)
    }

    const updatePublication = () => {
        setPublications(AuthenticationService.images)
    }

    const closeModalEdit = () => {
        setOpenEdit(false)
    }
    const closeModalDeletePublication = () => {
        setOpenDeletePublication(false)
    }
    const closeModalEditPublication = () => {
        setOpenEditPublication(false)
    }
    const handleExpandClick = () => {
        setExpanded(!expanded);
    };
    return (
        <div style={{ minHeight: '100vh', maxHeight: "500vh" }} className={classes.fond}>
            <NavBar />
            <br />
            <Grid container justifyContent='center' alignItems='center' alignContent='center' style={{ height: '10vh' }}>
                <Grid item xs={12}>
                    <h1>Bienvenue {AuthenticationService.firstName} {AuthenticationService.nom}</h1>
                </Grid>
            </Grid>
            <Grid container justifyContent='center' alignItems='center' alignContent='center' style={{ height: '10vh' }}>
                <Grid item xs={12}>
                    <h2> Mes publications</h2>
                </Grid>
                <Grid item xs={3}>
                    <Button variant="contained" onClick={() => { setOpen(true) }}>+</Button>
                </Grid>
                <Grid item xs={3}><Button variant="contained" onClick={() => { setOpenEdit(true) }}><AssignmentIndIcon /></Button></Grid>

            </Grid>
            <Grid container justifyContent='center' alignItems='center' alignContent='center' style={{ minHeight: '60vh' }}>
                {
                    publications.map((pub, index) => {
                        return (
                            <Grid item xs={9} md={5} lg={3} style={{ width: "100%", margin: "0 auto" }}>
                                <Card >
                                    <CardHeader
                                        title={pub.titre}
                                        subheader={pub.date}
                                    />
                                    <CardMedia
                                        component="img"
                                        height="194"
                                        image={pub.image}
                                        alt="Paella dish"
                                    />
                                    <CardContent>
                                        <Typography variant="body2" >
                                            {pub.description}
                                        </Typography>
                                    </CardContent>
                                    <CardActions disableSpacing>
                                        <IconButton aria-label="add to favorites">
                                            <ThumbUpAltIcon />
                                        </IconButton>
                                        <IconButton aria-label="share">
                                            {pub.like}
                                        </IconButton>
                                        <IconButton aria-label="Edit" onClick={() => { setOpenEditPublication(true) }}>
                                            <EditIcon />
                                        </IconButton>
                                        <IconButton aria-label="Delete" onClick={() => { setOpenDeletePublication(true) }}>
                                            <DeleteIcon />
                                        </IconButton>
                                    </CardActions>
                                </Card>
                                <EditPublication open={openEditPublication} updatePublication={updatePublication} closeModal={closeModalEditPublication} date={pub.date} description={pub.description} like={pub.like} image={pub.image} titre={pub.titre} index={index} />
                                <DeletePublication open={openDeletePublication} updatePublication={updatePublication} closeModal={closeModalDeletePublication} date={pub.date} description={pub.description} like={pub.like} image={pub.image} titre={pub.titre} index={index} />

                            </Grid>
                        )
                    })}
            </Grid>
            <AddPublication open={open} closeModal={closeModal} updatePublication={updatePublication} />
            <EditAccount open={openEdit} closeModal={closeModalEdit} />
        </div>



    );
}

export default Publication;
