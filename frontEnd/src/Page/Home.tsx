import '../App.css';
import { Button, Tooltip, Container, Paper, Avatar, Typography } from '@material-ui/core';
import { TextField } from '@material-ui/core';
import { PATHS } from '../routes';
import { Form, Formik, FormikProps } from 'formik';
import * as Yup from 'yup';
import { makeStyles } from '@material-ui/core/styles';
import LockOutlinedIcon from '@material-ui/icons/LockOutlined';
import AuthenticationService from '../auth';
import { useHistory } from 'react-router-dom';
import PersonAddIcon from '@material-ui/icons/PersonAdd';
import { AddAccount } from '../Composant/addAccount';
import { useState } from 'react';
import { useSnackbar } from "notistack";
import { ValidAccount } from '../Composant/ValidEmail';
import { LostPassword } from '../Composant/lostPassword';
import { ChangePassword } from '../Composant/ChangePassword';

const useStyles = makeStyles((theme) => ({
    paper: {
        marginTop: theme.spacing(10),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    logo: {
        marginTop: theme.spacing(4),
        marginBottom: theme.spacing(4),
        display: 'flex',
        flexDirection: 'column',
        alignItems: 'center',
    },
    titre: {
        marginTop: theme.spacing(8),
        color: "black",
    },
    container: {
        marginTop: theme.spacing(5),
    },
    avatar: {
        margin: theme.spacing(1),
        backgroundColor: "#F8A055",
    },
    error: {
        backgroundColor: "red",
        color: "white",
        textAlign: "center"
    },
    form: {
        width: '100%', // Fix IE 11 issue.
        marginTop: theme.spacing(1),
        marginBottom: theme.spacing(3),
    },
    fond: {
        backgroundColor: "black",
        backgroundRepeat: "no-repeat",
        backgroundSize: "cover",

    },
    submit: {
        margin: theme.spacing(3, 0, 2),
    },
    createAccount: {
        marginRight: theme.spacing(3),
        float: "right"
    }
}));


interface ConnexionForm {
    email: string
    password: string
}


const Home = () => {

    const classes = useStyles();
    const history = useHistory();
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();

    const [openaddAccount, setOpenAddAccount] = useState(false)
    const [openvalidAccount, setOpenValidAccount] = useState(false)
    const [openLostPassword, setOpenLostPassword] = useState(false)
    const [openChangePassword, setOpenChangePassword] = useState(false)


    const closeModal = () => {
        setOpenAddAccount(false)
    }
    const closeSendModal = () => {
        setOpenAddAccount(false)
        setOpenValidAccount(true)
    }
    const closeModalValidAccount = () => {
        setOpenValidAccount(false)
    }
    const closeModalLostPassword = () => {
        setOpenLostPassword(false)
    }
    const closeModalSendLostPassword = () => {
        setOpenLostPassword(false)
        setOpenChangePassword(true)
    }
    const closeModalChangePassword = () => {
        setOpenChangePassword(false)
    }
    return (
        <div style={{ height: '100vh' }} className={classes.fond}>
            <br />
            <Button variant="contained"
                color="primary"
                onClick={() => { setOpenAddAccount(true) }} className={classes.createAccount}>
                <Tooltip title="Créer un nouveau compte" arrow placement="top">
                    <PersonAddIcon />
                </Tooltip>
            </Button>
            <Container maxWidth="xs">

                <Paper className={classes.paper} elevation={0} variant="outlined"  >
                    {/* <img src={logo} alt="Logo 4CAST" width="200" className={classes.logo} /> */}
                    <Avatar className={classes.avatar}>
                        <LockOutlinedIcon />
                    </Avatar>
                    <Typography component="h1" variant="h5">
                        Se connecter
                    </Typography>


                    <Formik
                        initialValues={{
                            password: '',
                            email: '',
                        }}
                        onSubmit={(values: ConnexionForm, actions) => {
                            let infoKey = enqueueSnackbar("Authentification en cours", {
                                variant: "info",
                                autoHideDuration: 5000,
                            });

                            AuthenticationService.login(values.email, values.password).then(isAuthenticated => {
                                closeSnackbar(infoKey)
                                if (!isAuthenticated || !AuthenticationService.isValid) {
                                    enqueueSnackbar("Le mot de passe ou l'email n'est pas valide", {
                                        variant: "error",
                                        autoHideDuration: 3000,
                                    });

                                    return;
                                }
                                else {
                                    enqueueSnackbar("Authentification réussite", {
                                        variant: "success",
                                        autoHideDuration: 3000,
                                    });

                                    history.push(PATHS.ESPACE)
                                }
                            }).catch((e) => {
                                // try {
                                //     setErreurLabel(e.response.data.response)
                                // }
                                // catch (e) {
                                //     setErreurLabel("")
                                // }
                                // handleClickOpen();
                            });

                        }}
                        validationSchema={Yup.object().shape({
                            email: Yup.string()
                                .email()
                                .required('Entrez un email valide'),
                            password: Yup.string()
                                .matches(
                                    /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*()/]).{8,20}\S$/
                                )
                                .required(
                                    'Entrez un mot de passe valide'
                                ),

                        })}
                    >

                        {(props: FormikProps<ConnexionForm>) => {
                            const {
                                values,
                                touched,
                                errors,
                                handleBlur,
                                handleChange
                            } = props
                            return (


                                <Form className={classes.form} noValidate>
                                    <Container>
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="email"
                                            value={values.email}
                                            label="Adresse email"
                                            name="email"
                                            autoComplete="email"
                                            autoFocus
                                            type="email"
                                            helperText={
                                                errors.email && touched.email
                                                    ? errors.email
                                                    : ''
                                            }
                                            error={
                                                errors.email && touched.email
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            name="password"
                                            label="mot de passe"
                                            value={values.password}
                                            type="password"
                                            id="password"
                                            autoComplete="current-password"
                                            helperText={
                                                errors.password && touched.password
                                                    ? 'Entrez votre mot de passe'
                                                    : ''
                                            }
                                            error={
                                                errors.password && touched.password
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                        <a onClick={() => { setOpenLostPassword(true) }}>mot de passe oublié</a>
                                    </Container>
                                    <Container className={classes.container}>
                                        <Button
                                            fullWidth
                                            variant="contained"
                                            color="primary"
                                            type="submit"
                                        >
                                            Connexion
                                        </Button>
                                    </Container>
                                </Form>
                            )
                        }}
                    </Formik>
                </Paper>
            </Container>
            <AddAccount closeModal={closeModal} open={openaddAccount} send={closeSendModal} />
            <ValidAccount closeModal={closeModalValidAccount} open={openvalidAccount} />
            <LostPassword closeModal={closeModalLostPassword} open={openLostPassword} send={closeModalSendLostPassword} />
            <ChangePassword closeModal={closeModalChangePassword} open={openChangePassword} />
        </div>


    );
}

export default Home;
