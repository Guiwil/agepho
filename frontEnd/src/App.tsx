import './App.css';
import Content from './Content';

const App = () => {
  return (
    <div className="App" style={{ height: '100%' }}>
      <Content />
    </div>

  );
}

export default App;
