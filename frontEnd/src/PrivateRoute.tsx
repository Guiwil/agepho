import React from 'react';
import { Route, Redirect } from 'react-router-dom';
import { PATHS } from './routes';
import AuthenticationService from './auth';

const PrivateRoute = ({ component: Component, ...rest }: any) => (
    <Route {...rest} render={(props) => {
        const isAuthenticated = AuthenticationService.isAuthenticated;
        const isValid = AuthenticationService.isValid;

        if (!isAuthenticated && !isValid) {
            return <Redirect to={PATHS.HOME} />
        }

        return <Component {...props} />
    }} />
);

export default PrivateRoute;
