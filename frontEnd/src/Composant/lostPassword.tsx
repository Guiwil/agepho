import React, { useEffect } from "react";
import * as sha512 from 'js-sha512';
import Button from "@material-ui/core/Button";
import {
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Tooltip,
    Select,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";
import { useState } from "react";
import TextField from "@material-ui/core/TextField";
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from "@material-ui/pickers";
import Close from "@material-ui/icons/Close";
import Save from "@material-ui/icons/Save";
import Calculate from "@material-ui/icons/Replay";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import axios from "axios";
import { useSnackbar } from "notistack";
import ImportExportIcon from "@material-ui/icons/ImportExport";

const apiUrl = process.env.REACT_APP_API_URL || "";
const apiKey = process.env.REACT_APP_API_KEY || "";

const useStyles = makeStyles((theme) => ({
    addUser: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
    editJ_button: {
        marginRight: theme.spacing(2),
    },
    AjoutPForm: {
        marginBottom: theme.spacing(3),
    },
    AjoutPForm_fin: {
        marginBottom: theme.spacing(2),
        textAlign: "center",
    },
    AjoutPForm_button: {
        marginRight: theme.spacing(2),
    },
    save: {
        marginRight: theme.spacing(4),
    },
    ucesoOpenList: {
        marginBottom: theme.spacing(0.5),
    },
    spaceBetweenUceso: {
        marginBottom: theme.spacing(1),
    },
    error: {
        backgroundColor: "red",
        color: "white",
        textAlign: "center",
    },
}));


export const LostPassword = (props: any) => {
    const classes = useStyles();
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();


    return (
        <Dialog
            fullWidth={true}
            maxWidth="md"
            open={props.open}
            onClose={props.closeModal}
            aria-labelledby="draggable-dialog-title"
        >
            <DialogTitle style={{ textAlign: "center" }}>
                Mot de passe oublié
            </DialogTitle>
            <DialogContent>
                <Grid
                    container
                    alignItems="center"
                    alignContent="center"
                    justifyContent="center"
                >
                    <Formik
                        initialValues={{ email: "" }}
                        onSubmit={(values, { resetForm }) => {
                            let infoKey = enqueueSnackbar("Ajout en cours", {
                                variant: "info",
                                autoHideDuration: 5000,
                            });

                            axios.post('http://127.0.0.1:6868/lostPassword', { "email": values.email }).then((data) => {
                                closeSnackbar(infoKey)
                                if (data.data !== "OK") {
                                    enqueueSnackbar("Un problème est survenu", {
                                        variant: "error",
                                        autoHideDuration: 3000,
                                    });
                                }
                                else {
                                    enqueueSnackbar("Email envoyé", {
                                        variant: "success",
                                        autoHideDuration: 3000,
                                    });
                                    props.send()
                                }

                            })
                            resetForm();
                        }}
                        validationSchema={Yup.object().shape({
                            email: Yup.string()
                                .required("L'email est manquant")
                        })}
                    >
                        {({ values, errors, touched, handleChange, handleBlur }) => (
                            <Form>
                                <Grid
                                    container
                                    alignItems="center"
                                    alignContent="center"
                                    justifyContent="center"
                                >
                                    <Grid item xs={12} sm={12}>
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="email"
                                            value={values.email}
                                            label="Adresse email"
                                            name="email"
                                            autoComplete="email"
                                            autoFocus
                                            type="email"
                                            helperText={
                                                errors.email && touched.email
                                                    ? errors.email
                                                    : ''
                                            }
                                            error={
                                                errors.email && touched.email
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid
                                    container
                                    alignItems="center"
                                    alignContent="center"
                                    justifyContent="center"
                                >
                                    <Grid item xs={6} md={4} className={classes.addUser}>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            size="small"
                                            type="submit"
                                        >
                                            Valider
                                        </Button>
                                    </Grid>
                                    <Grid
                                        item
                                        xs={6}
                                        md={4}
                                        className={classes.addUser}
                                        style={{ textAlign: "center" }}
                                    >
                                        <Button
                                            size="small"
                                            variant="contained"
                                            onClick={() => props.closeModal()}
                                            color="secondary"
                                        >
                                            Fermer
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Form>
                        )}
                    </Formik>
                </Grid>
            </DialogContent>
        </Dialog>
    );
};
