import { Dialog, DialogTitle, DialogContent, TextField, DialogActions, Grid, Button, Container, Fab, makeStyles, CardMedia } from "@material-ui/core";
import { Formik, FormikProps, Form } from "formik";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import AuthenticationService from "../auth";
import { PATHS } from "../routes";
import * as Yup from 'yup';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import { useSnackbar } from "notistack";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
    button: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2)
    },
    input: {
        display: "none"
    },
}))

interface ajoutForm {
    titre: string
    description: string,
    image: string
}

export const DeletePublication = (props: any) => {

    const classes = useStyles();
    const history = useHistory();
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();


    const close = () => {
        props.closeModal()
    }

    return (
        props.open &&
        <Dialog
            open={props.open}
            onClose={props.closeModal}
            maxWidth="lg"
            fullWidth={true}
            aria-labelledby="draggable-dialog-title"
        >
            <DialogTitle style={{ textAlign: "center" }} >Supprimession de {props.titre}</DialogTitle>
            <DialogContent style={{ textAlign: "center" }}>

                <Formik
                    initialValues={{
                        titre: props.titre,
                        description: props.description,
                        image: ''
                    }}
                    onSubmit={(values: ajoutForm, actions) => {
                        console.log("ici")
                        let infoKey = enqueueSnackbar("Modification de l'utilisateur en cours", {
                            variant: "info",
                            autoHideDuration: 5000,
                        });
                        axios.get('http://127.0.0.1:6868/deletePublication?email=' + AuthenticationService.email + '&position=' + props.index).then((data) => {
                            closeSnackbar(infoKey)
                            console.log(data.data)
                            if (data.data === "Error") {
                                enqueueSnackbar("Un problème est survenu", {
                                    variant: "error",
                                    autoHideDuration: 3000,
                                });
                            }
                            else {
                                const user = data.data
                                console.log("ici")
                                console.log(user.images)
                                AuthenticationService.updatePublication(user.images).then((data) => {
                                    enqueueSnackbar("Publication mise à jour", {
                                        variant: "success",
                                        autoHideDuration: 3000,
                                    });
                                    props.updatePublication()
                                    close()
                                })
                            }

                        })
                    }}
                    validationSchema={Yup.object().shape({
                        titre: Yup.string()
                            .required('Entrez un titre'),
                        description: Yup.string()
                    })}
                >

                    {(props: FormikProps<ajoutForm>) => {
                        const {
                            values,
                            touched,
                            errors,
                            handleBlur,
                            handleChange
                        } = props
                        return (
                            <Form noValidate>
                                <Grid container alignContent="center" justify="center">
                                    <Grid item xs={12}>
                                        La suppresion de la publication sera définitive. En êtes vous sur ?
                                    </Grid>
                                </Grid>
                                <Grid container alignContent="center" justify="center" className={classes.button}>
                                    <Grid item xs={2}>

                                        <Button
                                            variant="contained"
                                            color="primary"
                                            type="submit"
                                        >
                                            Oui
                                        </Button>
                                    </Grid>
                                    <Grid item xs={2}>

                                        <Button variant="outlined" size='small' onClick={() => {
                                            close()
                                        }}>Non</Button>
                                    </Grid>
                                </Grid>
                            </Form>
                        )
                    }}
                </Formik>
            </DialogContent>
        </Dialog >


    )
}