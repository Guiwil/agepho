import { Dialog, DialogTitle, DialogContent, TextField, DialogActions, Grid, Button, Container, Fab, makeStyles, CardMedia } from "@material-ui/core";
import { Formik, FormikProps, Form } from "formik";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import AuthenticationService from "../auth";
import { PATHS } from "../routes";
import * as Yup from 'yup';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import { useSnackbar } from "notistack";
import axios from "axios";

const useStyles = makeStyles((theme) => ({
    button: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2)
    },
    input: {
        display: "none"
    },
}))

interface ajoutForm {
    titre: string
    description: string,
    image: string
}

export const EditPublication = (props: any) => {

    const classes = useStyles();
    const history = useHistory();
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();


    const [img, setImg] = useState(props.image)

    const close = () => {
        props.closeModal()
    }

    return (
        props.open &&
        <Dialog
            open={props.open}
            onClose={props.closeModal}
            maxWidth="lg"
            fullWidth={true}
            aria-labelledby="draggable-dialog-title"
        >
            <DialogTitle style={{ textAlign: "center" }} >Ajouter une publication</DialogTitle>
            <DialogContent style={{ textAlign: "center" }}>

                <Formik
                    initialValues={{
                        titre: props.titre,
                        description: props.description,
                        image: ''
                    }}
                    onSubmit={(values: ajoutForm, actions) => {
                        console.log("ici")
                        let infoKey = enqueueSnackbar("Modification de l'utilisateur en cours", {
                            variant: "info",
                            autoHideDuration: 5000,
                        });
                        axios.post('http://127.0.0.1:6868/editPublication', { "email": AuthenticationService.email, "image": { "titre": values.titre, "description": values.description, "image": img, date: props.date, "like": props.like }, position: props.index }).then((data) => {
                            closeSnackbar(infoKey)
                            console.log(data.data)
                            if (data.data === "User not found") {
                                enqueueSnackbar("Un problème est survenu", {
                                    variant: "error",
                                    autoHideDuration: 3000,
                                });
                            }
                            else {
                                const user = data.data
                                console.log("ici")
                                console.log(user.images)
                                AuthenticationService.updatePublication(user.images).then((data) => {
                                    enqueueSnackbar("Publication mise à jour", {
                                        variant: "success",
                                        autoHideDuration: 3000,
                                    });
                                    props.updatePublication()
                                    close()
                                })
                            }

                        })
                    }}
                    validationSchema={Yup.object().shape({
                        titre: Yup.string()
                            .required('Entrez un titre'),
                        description: Yup.string()
                    })}
                >

                    {(props: FormikProps<ajoutForm>) => {
                        const {
                            values,
                            touched,
                            errors,
                            handleBlur,
                            handleChange
                        } = props
                        return (
                            <Form noValidate>
                                <Grid container alignContent="center" justify="center">
                                    <Grid item xs={7}>
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="titre"
                                            value={values.titre}
                                            label="Titre"
                                            name="titre"
                                            autoFocus
                                            helperText={
                                                errors.titre && touched.titre
                                                    ? errors.titre
                                                    : ''
                                            }
                                            error={
                                                errors.titre && touched.titre
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                    <Grid item xs={7} className={classes.button}>
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            multiline
                                            rows={4}
                                            name="description"
                                            label="Description"
                                            value={values.description}
                                            id="description"
                                            helperText={
                                                errors.description && touched.description
                                                    ? 'Entrez une description'
                                                    : ''
                                            }
                                            error={
                                                errors.description && touched.description
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                    <Grid item xs={7}>
                                        <Grid container alignContent="center" justify="center">
                                            <Grid item xs={9}>
                                                {
                                                    img &&
                                                    <CardMedia
                                                        component="img"
                                                        height="194"
                                                        image={img}
                                                        alt="Paella dish"
                                                    />
                                                }
                                                {
                                                    !img &&
                                                    <CardMedia
                                                        component="img"
                                                        height="194"
                                                        image="http://via.placeholder.com/640x360"
                                                        alt="Paella dish"
                                                    />
                                                }

                                            </Grid>
                                            <Grid item xs={3}>
                                                <input
                                                    accept="image/*,"
                                                    id="contained-button-file"
                                                    className={classes.input}
                                                    multiple
                                                    type="file"
                                                    onChange={(newFile) => {
                                                        const fileReader = new FileReader();
                                                        if (newFile.target.files !== null) {
                                                            fileReader.readAsDataURL(newFile.target.files[0]);
                                                            fileReader.onload = newFile => {

                                                                if (typeof (newFile.target?.result) === "string")
                                                                    setImg(newFile.target?.result)
                                                                console.log(values)

                                                            };
                                                        }
                                                    }}
                                                />
                                                <label htmlFor="contained-button-file">
                                                    <Fab component="span" >
                                                        <InsertDriveFileIcon />
                                                    </Fab>
                                                </label>
                                            </Grid>
                                        </Grid>
                                    </Grid>
                                </Grid>
                                <Grid container alignContent="center" justify="center" className={classes.button}>
                                    <Grid item xs={2}>

                                        <Button
                                            variant="contained"
                                            color="primary"
                                            type="submit"
                                        >
                                            Oui
                                        </Button>
                                    </Grid>
                                    <Grid item xs={2}>

                                        <Button variant="outlined" size='small' onClick={() => {
                                            close()
                                        }}>Non</Button>
                                    </Grid>
                                </Grid>
                            </Form>
                        )
                    }}
                </Formik>
            </DialogContent>
        </Dialog >


    )
}