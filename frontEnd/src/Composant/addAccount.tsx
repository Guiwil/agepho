import React, { useEffect } from "react";
import * as sha512 from 'js-sha512';
import Button from "@material-ui/core/Button";
import {
    Dialog,
    DialogActions,
    DialogContent,
    DialogContentText,
    DialogTitle,
    Tooltip,
    Select,
} from "@material-ui/core";
import { makeStyles } from "@material-ui/core/styles";
import Grid from "@material-ui/core/Grid";
import Table from "@material-ui/core/Table";
import TableBody from "@material-ui/core/TableBody";
import TableCell from "@material-ui/core/TableCell";
import TableContainer from "@material-ui/core/TableContainer";
import TableHead from "@material-ui/core/TableHead";
import TableRow from "@material-ui/core/TableRow";
import Paper from "@material-ui/core/Paper";
import Checkbox from "@material-ui/core/Checkbox";
import { useState } from "react";
import TextField from "@material-ui/core/TextField";
import {
    MuiPickersUtilsProvider,
    KeyboardDatePicker,
} from "@material-ui/pickers";
import Close from "@material-ui/icons/Close";
import Save from "@material-ui/icons/Save";
import Calculate from "@material-ui/icons/Replay";
import { Formik, Form } from "formik";
import * as Yup from "yup";
import FormControl from "@material-ui/core/FormControl";
import MenuItem from "@material-ui/core/MenuItem";
import InputLabel from "@material-ui/core/InputLabel";
import axios from "axios";
import { useSnackbar } from "notistack";
import ImportExportIcon from "@material-ui/icons/ImportExport";

const apiUrl = process.env.REACT_APP_API_URL || "";
const apiKey = process.env.REACT_APP_API_KEY || "";

const useStyles = makeStyles((theme) => ({
    addUser: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2),
    },
    editJ_button: {
        marginRight: theme.spacing(2),
    },
    AjoutPForm: {
        marginBottom: theme.spacing(3),
    },
    AjoutPForm_fin: {
        marginBottom: theme.spacing(2),
        textAlign: "center",
    },
    AjoutPForm_button: {
        marginRight: theme.spacing(2),
    },
    save: {
        marginRight: theme.spacing(4),
    },
    ucesoOpenList: {
        marginBottom: theme.spacing(0.5),
    },
    spaceBetweenUceso: {
        marginBottom: theme.spacing(1),
    },
    error: {
        backgroundColor: "red",
        color: "white",
        textAlign: "center",
    },
}));


export const AddAccount = (props: any) => {
    const classes = useStyles();
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();


    return (
        <Dialog
            fullWidth={true}
            maxWidth="md"
            open={props.open}
            onClose={props.closeModal}
            aria-labelledby="draggable-dialog-title"
        >
            <DialogTitle style={{ textAlign: "center" }}>
                S'inscrire
            </DialogTitle>
            <DialogContent>
                <Grid
                    container
                    alignItems="center"
                    alignContent="center"
                    justifyContent="center"
                >
                    <Formik
                        initialValues={{ nom: "", firstName: "", email: "", password: "", verifPassword: "" }}
                        onSubmit={(values, { resetForm }) => {
                            let infoKey = enqueueSnackbar("Ajout en cours", {
                                variant: "info",
                                autoHideDuration: 5000,
                            });
                            var pwd = sha512.sha512(values.password);
                            axios.post('http://127.0.0.1:6868/addUser', { "email": values.email, "password": pwd, "firstName": values.firstName, "name": values.nom }).then((data) => {
                                closeSnackbar(infoKey)
                                if (data.data !== "User add") {
                                    enqueueSnackbar("Un problème est survenu", {
                                        variant: "error",
                                        autoHideDuration: 3000,
                                    });
                                }
                                else {
                                    enqueueSnackbar("Utilisateur ajouté", {
                                        variant: "success",
                                        autoHideDuration: 3000,
                                    });
                                    props.send()
                                }

                            })
                            resetForm();
                        }}
                        validationSchema={Yup.object().shape({
                            nom: Yup.string()
                                .required("Le titre est manquant"),
                            password: Yup.string()
                                .matches(
                                    /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*()/]).{8,20}\S$/, "Merci de mettre au moins une majuscule, un nombre, un caractère spéciale"
                                )
                                .required(
                                    'Entrez un mot de passe valide'
                                ),
                            verifPassword: Yup.string()
                                .oneOf([Yup.ref('password'), null], 'Mot de passe différent')
                        })}
                    >
                        {({ values, errors, touched, handleChange, handleBlur }) => (
                            <Form>
                                <Grid
                                    container
                                    alignItems="center"
                                    alignContent="center"
                                    justifyContent="center"
                                >
                                    <Grid item xs={12} sm={8}>
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="email"
                                            value={values.email}
                                            label="Adresse email"
                                            name="email"
                                            autoComplete="email"
                                            autoFocus
                                            type="email"
                                            helperText={
                                                errors.email && touched.email
                                                    ? errors.email
                                                    : ''
                                            }
                                            error={
                                                errors.email && touched.email
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={5} className={classes.save}>
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="firstName"
                                            value={values.nom}
                                            label="Nom"
                                            name="nom"
                                            autoComplete="nom"
                                            helperText={
                                                errors.nom && touched.nom
                                                    ? errors.nom
                                                    : ''
                                            }
                                            error={
                                                errors.nom && touched.nom
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={5} >
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="firstName"
                                            value={values.firstName}
                                            label="Prénom"
                                            name="firstName"
                                            autoComplete="firstName"
                                            helperText={
                                                errors.nom && touched.nom
                                                    ? errors.nom
                                                    : ''
                                            }
                                            error={
                                                errors.nom && touched.nom
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={5} className={classes.save}>
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="password"
                                            value={values.password}
                                            label="Mot de passe"
                                            name="password"
                                            autoComplete="password"
                                            type="password"
                                            helperText={
                                                errors.password && touched.password
                                                    ? errors.password
                                                    : ''
                                            }
                                            error={
                                                errors.password && touched.password
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                    <Grid item xs={12} sm={5}>
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="verifPassword"
                                            value={values.verifPassword}
                                            label="Validation Mot de passe"
                                            name="verifPassword"
                                            autoComplete="verifPassword"
                                            type="password"
                                            helperText={
                                                errors.verifPassword && touched.verifPassword
                                                    ? errors.verifPassword
                                                    : ''
                                            }
                                            error={
                                                errors.verifPassword && touched.verifPassword
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid
                                    container
                                    alignItems="center"
                                    alignContent="center"
                                    justifyContent="center"
                                >
                                    <Grid item xs={6} md={3} className={classes.addUser}>
                                        <Button
                                            variant="contained"
                                            color="primary"
                                            size="small"
                                            type="submit"
                                        >
                                            Valider
                                        </Button>
                                    </Grid>
                                    <Grid
                                        item
                                        xs={6}
                                        md={3}
                                        className={classes.addUser}
                                        style={{ textAlign: "center" }}
                                    >
                                        <Button
                                            size="small"
                                            variant="contained"
                                            onClick={() => props.closeModal()}
                                            color="secondary"
                                        >
                                            Fermer
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Form>
                        )}
                    </Formik>
                </Grid>
            </DialogContent>
        </Dialog>
    );
};
