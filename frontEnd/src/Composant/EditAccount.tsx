import { Dialog, DialogTitle, DialogContent, TextField, DialogActions, Grid, Button, Container, Fab, makeStyles, CardMedia, Divider } from "@material-ui/core";
import { Formik, FormikProps, Form } from "formik";
import { useState } from "react";
import { useHistory } from "react-router-dom";
import AuthenticationService from "../auth";
import { PATHS } from "../routes";
import * as Yup from 'yup';
import InsertDriveFileIcon from '@material-ui/icons/InsertDriveFile';
import axios from "axios";
import { useSnackbar } from "notistack";
import * as sha512 from 'js-sha512';

const useStyles = makeStyles((theme) => ({
    button: {
        marginTop: theme.spacing(2),
        marginBottom: theme.spacing(2)
    },
    ecart: {
        marginRight: theme.spacing(2)
    },
    input: {
        display: "none"
    }
}))

interface ajoutForm {
    name: string
    firstName: string
}

export const EditAccount = (props: any) => {

    const classes = useStyles();
    const { enqueueSnackbar, closeSnackbar } = useSnackbar();
    const [name, setName] = useState(AuthenticationService.nom)
    const [firstName, setFirstName] = useState(AuthenticationService.firstName)
    const history = useHistory();

    const close = () => {
        props.closeModal()
    }

    return (
        props.open &&
        <Dialog
            open={props.open}
            onClose={props.closeModal}
            maxWidth="lg"
            fullWidth={true}
            aria-labelledby="draggable-dialog-title"
        >
            <DialogTitle style={{ textAlign: "center" }} >Modification de {name} {firstName}</DialogTitle>
            <DialogContent style={{ textAlign: "center" }}>
                <Formik
                    initialValues={{
                        name: name,
                        firstName: firstName
                    }}
                    onSubmit={(values, { resetForm }) => {
                        console.log("ici")
                        let infoKey = enqueueSnackbar("Modification de l'utilisateur en cours", {
                            variant: "info",
                            autoHideDuration: 5000,
                        });
                        axios.post('http://127.0.0.1:6868/editUser', { "email": AuthenticationService.email, "firstName": values.firstName, "name": values.name }).then((data) => {
                            closeSnackbar(infoKey)
                            console.log(data.data)
                            if (data.data === "Utilisateur non trouve") {
                                enqueueSnackbar("Un problème est survenu", {
                                    variant: "error",
                                    autoHideDuration: 3000,
                                });
                            }
                            else {
                                const user = data.data
                                AuthenticationService.updateUser(user.firstName, user.name).then((data) => {
                                    setName(AuthenticationService.nom)
                                    setFirstName(AuthenticationService.firstName)
                                    enqueueSnackbar("Utilisateur modifié", {
                                        variant: "success",
                                        autoHideDuration: 3000,
                                    });
                                    close()
                                })
                            }

                        })
                    }}
                    validationSchema={Yup.object().shape({
                        firstName: Yup.string()
                            .required('Entrez un prénom'),
                        name: Yup.string().required('Entrez un nom')
                    })}
                >

                    {(props: FormikProps<ajoutForm>) => {
                        const {
                            values,
                            touched,
                            errors,
                            handleBlur,
                            handleChange
                        } = props
                        return (
                            <Form noValidate>
                                <Grid container alignContent="center" justifyContent="center">
                                    <Grid item xs={4}></Grid>
                                    <Grid item xs={4}>
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="name"
                                            value={values.name}
                                            label="Name"
                                            name="name"
                                            autoFocus
                                            helperText={
                                                errors.name && touched.name
                                                    ? errors.name
                                                    : ''
                                            }
                                            error={
                                                errors.name && touched.name
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                    <Grid item xs={4}></Grid>
                                    <Grid item xs={4}></Grid>
                                    <Grid item xs={4} className={classes.button}>
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="firstName"
                                            value={values.firstName}
                                            label="Prénom"
                                            name="firstName"
                                            autoFocus
                                            helperText={
                                                errors.firstName && touched.firstName
                                                    ? errors.firstName
                                                    : ''
                                            }
                                            error={
                                                errors.firstName && touched.firstName
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                    <Grid item xs={4}></Grid>
                                </Grid>
                                <Grid container alignContent="center" justifyContent="center" className={classes.button}>
                                    <Grid item xs={2}>

                                        <Button
                                            variant="contained"
                                            color="primary"
                                            type="submit"
                                        >
                                            Oui
                                        </Button>
                                    </Grid>
                                </Grid>
                            </Form>
                        )
                    }}
                </Formik>
                <Divider></Divider>
                <Formik
                    initialValues={{
                        actualPassword: "",
                        newPassword: "",
                        checkNewPassword: ""
                    }}
                    onSubmit={(values, { resetForm }) => {
                        console.log("ici")
                        let infoKey = enqueueSnackbar("Modification de l'utilisateur en cours", {
                            variant: "info",
                            autoHideDuration: 5000,
                        });
                        var actual = sha512.sha512(values.actualPassword);
                        var newPass = sha512.sha512(values.newPassword);

                        axios.post('http://127.0.0.1:6868/changePassord', { "email": AuthenticationService.email, "actualPassword": actual, "password": newPass }).then((data) => {
                            closeSnackbar(infoKey)
                            console.log(data.data)
                            if (data.data === "Utilisateur non trouve") {
                                enqueueSnackbar("Un problème est survenu", {
                                    variant: "error",
                                    autoHideDuration: 3000,
                                });
                            }
                            else {
                                const user = data.data
                                AuthenticationService.updateUser(user.firstName, user.name).then((data) => {
                                    setName(AuthenticationService.nom)
                                    setFirstName(AuthenticationService.firstName)
                                    enqueueSnackbar("Utilisateur modifié", {
                                        variant: "success",
                                        autoHideDuration: 3000,
                                    });
                                    close()
                                })
                            }

                        })
                    }}
                    validationSchema={Yup.object().shape({
                        actualPassword: Yup.string()
                            .matches(
                                /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*()/]).{8,20}\S$/, "Merci de mettre au moins une majuscule, un nombre, un caractère spéciale"
                            )
                            .required(
                                'Entrez un mot de passe valide'
                            ),
                        newPassword: Yup.string()
                            .matches(
                                /^(?=.*[A-Z])(?=.*[a-z])(?=.*[0-9])(?=.*[!@#$%^&*()/]).{8,20}\S$/, "Merci de mettre au moins une majuscule, un nombre, un caractère spéciale"
                            )
                            .required(
                                'Entrez un mot de passe valide'
                            ),
                        checkNewPassword: Yup.string()
                            .oneOf([Yup.ref('newPassword'), null], 'Mot de passe différent')
                    })}
                >

                    {(props: FormikProps<any>) => {
                        const {
                            values,
                            touched,
                            errors,
                            handleBlur,
                            handleChange
                        } = props
                        return (
                            <Form noValidate>
                                <Grid container alignContent="center" justifyContent="center">
                                    <Grid item xs={8}>
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            id="actualPassword"
                                            value={values.actualPassword}
                                            label="actualPassword"
                                            name="actualPassword"
                                            autoFocus
                                            helperText={
                                                errors.actualPassword && touched.actualPassword
                                                    ? errors.actualPassword
                                                    : ''
                                            }
                                            error={
                                                errors.actualPassword && touched.actualPassword
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={5} className={classes.ecart}>
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="newPassword"
                                            value={values.newPassword}
                                            label="Nouveau mot de passe"
                                            name="newPassword"
                                            type="password"
                                            helperText={
                                                errors.newPassword && touched.newPassword
                                                    ? errors.newPassword
                                                    : ''
                                            }
                                            error={
                                                errors.newPassword && touched.newPassword
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                    <Grid item xs={12} md={5} >
                                        <TextField
                                            variant="outlined"
                                            margin="normal"
                                            required
                                            fullWidth
                                            id="checkNewPassword"
                                            value={values.checkNewPassword}
                                            label="Validation mot de passe"
                                            name="checkNewPassword"
                                            type="password"
                                            helperText={
                                                errors.checkNewPassword && touched.checkNewPassword
                                                    ? errors.checkNewPassword
                                                    : ''
                                            }
                                            error={
                                                errors.checkNewPassword && touched.checkNewPassword
                                                    ? true
                                                    : false
                                            }
                                            onChange={handleChange}
                                            onBlur={handleBlur}
                                        />
                                    </Grid>
                                </Grid>
                                <Grid container alignContent="center" justifyContent="center" className={classes.button}>
                                    <Grid item xs={2}>

                                        <Button
                                            variant="contained"
                                            color="primary"
                                            type="submit"
                                        >
                                            Oui
                                        </Button>
                                    </Grid>
                                    <Grid item xs={2}>

                                        <Button variant="outlined" size='small' onClick={() => {
                                            close()
                                        }}>Non</Button>
                                    </Grid>
                                </Grid>
                            </Form>
                        )
                    }}
                </Formik>
            </DialogContent>
        </Dialog >


    )
}