
import * as sha512 from 'js-sha512';
import axios from 'axios';

export default class AuthenticationService {

    static isAuthenticated: boolean = false;
    static email: string;
    static nom: string;
    static firstName: string;
    static images: any[];
    static token: string;
    static isValid: boolean = false;
    static like: any[];

    static async login(email: string, password: string): Promise<boolean> {
        var isAuthenticated = false;
        var pwd = sha512.sha512(password);

        console.log("ici")

        await axios.post('http://127.0.0.1:6868/login', { "email": email, "password": pwd }).then((data) => {
            console.log("bbbbbb")

            if (data.data !== "Utilisateur non trouve") {
                console.log(data.data)
                var user = data.data
                isAuthenticated = true
                this.email = email
                this.firstName = user.firstName
                this.nom = user.name
                this.images = user.images
                this.like = user.like
                this.isValid = user.isValid
            }
        })

        return new Promise(resolve => {
            this.isAuthenticated = isAuthenticated;
            resolve(isAuthenticated);

        })
    };
    static async logout(): Promise<boolean> {


        this.isAuthenticated = false;
        this.email = "";
        this.nom = "";
        this.firstName = ""
        this.images = []
        return new Promise(resolve => {
            resolve(true);

        })
    };

    static async updateUser(firstName: string, name: string): Promise<boolean> {

        this.firstName = firstName
        this.nom = name
        return new Promise(resolve => {
            resolve(true);

        })
    };
    static async updatePublication(publication: any): Promise<boolean> {

        this.images = publication
        return new Promise(resolve => {
            resolve(true);

        })
    };
    static async updateLike(like: any): Promise<boolean> {

        this.like = like
        return new Promise(resolve => {
            resolve(true);

        })
    };
}
