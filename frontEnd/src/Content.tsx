import React from 'react';
import { Switch, Route } from 'react-router-dom';
import { PATHS } from './routes';

import Home from './Page/Home';
import Espace from './Page/Espace';
import PrivateRoute from './PrivateRoute';
import Account from './Page/Account';
import Publication from './Page/Publication';
import ValidEmail from './Page/validEmail';
import ChangePassword from './Page/ChangePassword';

const Content: React.FC<{}> = () => {

    return (
        <Switch>
            <Route exact path={PATHS.HOME} component={Home} />
            <Route exact path={PATHS.VALIDEMAIL} component={ValidEmail} />
            <Route exact path={PATHS.CHANGEPASSWORD} component={ChangePassword} />
            <PrivateRoute exact path={PATHS.ESPACE} component={Espace} />
            <PrivateRoute exact path={PATHS.ACCOUNT} component={Account} />
            <PrivateRoute exact path={PATHS.PUBLICATION} component={Publication} />
        </Switch>
    );
};

export default Content;