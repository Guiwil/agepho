export enum PATHS {
    HOME = '/',
    ESPACE = '/Espace',
    ACCOUNT = '/Account',
    PUBLICATION = '/Publication',
    VALIDEMAIL = '/validEmail',
    CHANGEPASSWORD = '/lostPassword'
}
