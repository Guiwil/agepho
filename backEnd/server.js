require("dotenv").config();
const express = require('express');
const cookieParser = require('cookie-parser')
const bodyParser = require('body-parser');
const cors = require("cors");
const fs = require('fs');
const nodemailer = require('nodemailer');

const app = express();
app.use(cookieParser());
app.use(cors());
app.use(bodyParser.json({ limit: '50mb' }));
app.use(bodyParser.urlencoded({ extended: true }));

function shuffleArray(inputArray) {
    inputArray.sort(() => Math.random() - 0.5);
}

function generate() {
    return Math.floor((1 + Math.random()) * 0x10000)
        .toString(16)
        .substring(1);

}

// set port, listen for requests
const PORT = process.env.NODE_DOCKER_PORT || 8080;
app.get('/', (req, res) => {
    res.set('Content-Type', 'text/html');
    res.send('Hello world !!');
});
app.get('/api/Title', (req, res) => {
    console.log(req.query.title)
    res.send('Hello world !!');
})

app.get('/deleteUser', (req, res) => {
    const email = req.query.email
    var find = false;
    const newList = { "users": [] }

    fs.readFile('db.json', 'utf8', function (err, data) {
        const content = JSON.parse(data);
        console.log(content);
        if (content.users.length !== 0) {
            content.users.forEach((element) => {
                if (element.email == email) {
                    find = true
                }
                else {
                    newList.users.push(element)
                }
            })
        }

        if (find) {
            fs.writeFile('db.json', JSON.stringify(newList), function (err) {
                if (err) throw err;
                res.end("User delete");
            });
        }
        else {
            res.end("User don't exist");
        }
    })
});

app.get('/deletePublication', (req, res) => {
    try {
        const email = req.query.email
        const position = req.query.position
        var find = false;
        var user;
        var positionUser

        fs.readFile('db.json', 'utf8', function (err, data) {
            const content = JSON.parse(data);
            console.log(content);
            if (content.users.length !== 0) {
                content.users.forEach((element, index) => {
                    if (element.email == email) {
                        find = true
                        user = element
                        positionUser = index
                    }
                })
            }

            if (find) {
                user.images.splice(position, 1)
                console.log(user)
                content.users[positionUser] = user
                fs.writeFile('db.json', JSON.stringify(content), function (err) {
                    if (err) throw err;
                    res.end(JSON.stringify(user));
                });
            }
            else {
                res.end("Error");
            }
        })
    }
    catch (e) {
        res.end("Error");
    }

});

app.get('/randomPublication', (req, res) => {
    const email = req.query.email
    const publication = []

    fs.readFile('db.json', 'utf8', function (err, data) {
        const content = JSON.parse(data);
        console.log(content);
        if (content.users.length !== 1) {
            content.users.forEach((element, position) => {
                if (element.email != email) {
                    element.images.forEach((image, i) => {
                        publication.push({ image: image, positionEmail: position, position: i, user: { email: element.email, name: element.name, firstName: element.firstName } })

                    })
                }
            })
        }

        shuffleArray(publication)

        res.end(JSON.stringify(publication));
    })
});

app.post('/like', (req, res) => {
    try {
        const positionEmail = req.body.positionEmail;
        const position = req.body.position;
        const email = req.body.email;
        const imageId = req.body.imageId;
        var positionUser;
        var user;

        fs.readFile('db.json', 'utf8', function (err, data) {
            const content = JSON.parse(data);
            console.log(content);
            if (content.users.length !== 1) {
                content.users.forEach((element, index) => {
                    if (element.email == email) {
                        positionUser = index
                        user = element
                    }
                })
            }
            const newLike = [].concat(user.like)
            newLike.push(imageId)
            user.like = newLike
            content.users[positionEmail].images[position].like = content.users[positionEmail].images[position].like + 1
            content.users[positionUser] = user
            console.log(content)
            fs.writeFile('db.json', JSON.stringify(content), function (err) {
                if (err) throw err;
                res.end(JSON.stringify(user));
            });
        })
    }
    catch (e) {
        res.end("Error");
    }

});


app.post('/addPublication', (req, res) => {
    const email = req.body.email;
    const titre = req.body.titre;
    const image = req.body.image;
    const date = req.body.date
    const description = req.body.description

    var find = false;
    var user;
    var indice;
    fs.readFile('db.json', 'utf8', function (err, data) {
        const content = JSON.parse(data);
        console.log(content);
        if (content.users.length !== 0) {
            content.users.forEach((element, i) => {
                if (element.email == email) {
                    find = true
                    user = element
                    indice = i
                }
            })
        }

        if (find) {
            const newImage = { "id": generate(), "titre": titre, "description": description, "date": date, "image": image, "like": 0 }
            user.images.push(newImage)
            content.users[indice] = user
            fs.writeFile('db.json', JSON.stringify(content), function (err) {
                if (err) throw err;
                res.end(JSON.stringify(user));
            });
        }
        else {
            res.end("User not find");
        }
    });
});

app.post('/editPublication', (req, res) => {
    const email = req.body.email;
    const image = req.body.image;
    const position = req.body.position

    var find = false;
    var user;
    var indice;
    fs.readFile('db.json', 'utf8', function (err, data) {
        const content = JSON.parse(data);
        console.log(content);
        if (content.users.length !== 0) {
            content.users.forEach((element, i) => {
                if (element.email == email) {
                    find = true
                    user = element
                    indice = i
                }
            })
        }

        if (find) {
            user.images[position] = image
            content.users[indice] = user
            fs.writeFile('db.json', JSON.stringify(content), function (err) {
                if (err) throw err;
                res.end(JSON.stringify(user));
            });
        }
        else {
            res.end("User not found");
        }
    });
});

app.post('/addUser', (req, res) => {
    const email = req.body.email;
    const password = req.body.password;
    const firstName = req.body.firstName;
    const name = req.body.name;

    const confirmCode = generate()
    const newUser = { "email": email, "password": password, "firstName": firstName, "name": name, "images": [], "like": [], "isValid": false, "confirmCode": confirmCode, "resetCode": generate() }

    var find = false;
    fs.readFile('db.json', 'utf8', function (err, data) {
        const content = JSON.parse(data);
        console.log(content);
        if (content.users.length !== 0) {
            content.users.forEach((element) => {
                if (element.email == email) {
                    find = true
                }
            })
        }

        if (find) {
            res.end("User existant");
        }
        else {
            content.users.push(newUser)
            var transporter = nodemailer.createTransport({
                service: 'Gmail',
                host: "smtp.gmail.com",
                auth: {
                    user: 'omeiryounes@gmail.com',
                    pass: 'ttigevwrftlooloo'
                }
            });

            new Promise((rsv, rjt) => {
                transporter.sendMail({
                    from: 'noreply.monkenewha@gmail.com',
                    to: email,
                    subject: 'Création de compte',
                    html: `
                    <!DOCTYPE html>
    <html>
    
    <head>
    <title></title>   
    </head>
    
    <body>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td bgcolor="blue" align="center">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">Code de validation : `+ confirmCode + `</td>
                        <td align="center" style="border-radius: 3px;" bgcolor="blue"><a href="http://localhost:3000/#/validEmail?confirmCode=` + confirmCode + `" target="_blank" style="font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: black; text-decoration: none; color: black; text-decoration: none; padding: 15px 25px; border-radius: 2px; border: 1px solid black; display: inline-block;">Confirmer l'email</a></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>            
    </table>
    </body>
    
    </html>
                    `

                }, (error, response) => {
                    if (error) {
                        console.log(error);
                    } else {
                        console.log(response);
                    }
                    transporter.close();
                    fs.writeFile('db.json', JSON.stringify(content), function (err) {
                        if (err) throw err;
                        content.users.push(newUser)
                        res.end("User add");
                        rsv("Envoie Fini")
                    });
                })
            });
        }
    });
});

app.post('/lostPassword', (req, res) => {
    const email = req.body.email;
    const lostPassword = generate()
    var position;
    var find = false;
    fs.readFile('db.json', 'utf8', function (err, data) {
        const content = JSON.parse(data);
        console.log(content);
        if (content.users.length !== 0) {
            content.users.forEach((element, indexe) => {
                if (element.email == email) {
                    find = true
                    position = indexe;
                }
            })
        }

        if (find) {
            content.users[position].resetCode = lostPassword
            var transporter = nodemailer.createTransport({
                service: 'Gmail',
                host: "smtp.gmail.com",
                auth: {
                    user: 'omeiryounes@gmail.com',
                    pass: 'ttigevwrftlooloo'
                }
            });

            new Promise((rsv, rjt) => {
                transporter.sendMail({
                    from: 'noreply.monkenewha@gmail.com',
                    to: email,
                    subject: 'Mot de passe oublié',
                    html: `
                    <!DOCTYPE html>
    <html>
    
    <head>
    <title></title>   
    </head>
    
    <body>
    <table border="0" cellpadding="0" cellspacing="0" width="100%">
        <tr>
            <td bgcolor="blue" align="center">
                <table border="0" cellpadding="0" cellspacing="0" width="100%" style="max-width: 600px;">
                    <tr>
                        <td align="center" valign="top" style="padding: 40px 10px 40px 10px;">Code de validation : `+ lostPassword + `</td>
                        <td align="center" style="border-radius: 3px;" bgcolor="blue"><a href="http://localhost:3000/#/lostPassword?confirmCode=` + lostPassword + `" target="_blank" style="font-size: 20px; font-family: Helvetica, Arial, sans-serif; color: black; text-decoration: none; color: black; text-decoration: none; padding: 15px 25px; border-radius: 2px; border: 1px solid black; display: inline-block;">Confirmer l'email</a></td>
                    </tr>
                </table>
            </td>
        </tr>
        <tr>            
    </table>
    </body>
    
    </html>
                    `

                }, (error, response) => {
                    if (error) {
                        console.log(error);
                    } else {
                        console.log(response);
                    }
                    transporter.close();
                    fs.writeFile('db.json', JSON.stringify(content), function (err) {
                        if (err) throw err;
                        res.end("OK");
                        rsv("Envoie Fini")
                    });
                })
            });
        }
        else {
            res.end("Error");
        }
    });
});


app.post('/login', (req, res) => {
    const email = req.body.email;
    const password = req.body.password;


    var find = false;
    var user;
    fs.readFile('db.json', 'utf8', function (err, data) {
        try {
            const content = JSON.parse(data);
            console.log(content);
            if (content.users.length !== 0) {
                content.users.forEach((element) => {
                    if (element.email == email && element.password == password) {
                        find = true
                        user = JSON.stringify(element)
                    }
                })
            }

            if (find) {
                res.end(user);
            }
            else {
                res.end("Utilisateur non trouve");
            }
        }
        catch (e) {
            res.end("Utilisateur non trouve");
        }
    });

});

app.post('/editUser', (req, res) => {
    const name = req.body.name;
    const firstName = req.body.firstName;
    const email = req.body.email;


    var find = false;
    var user;
    var position;
    fs.readFile('db.json', 'utf8', function (err, data) {
        const content = JSON.parse(data);
        console.log(content);
        if (content.users.length !== 0) {
            content.users.forEach((element, index) => {
                if (element.email == email) {
                    find = true
                    position = index
                    user = element
                }
            })
        }

        if (find) {
            user.name = name
            user.firstName = firstName
            content.users[position] = user
            console.log(content)
            console.log(user)
            const finalData = JSON.stringify(content)
            console.log(finalData)
            fs.writeFile('db.json', finalData, function (err) {
                if (err) throw err;
                res.end(JSON.stringify(user));
            });
        }
        else {
            res.end("Utilisateur non trouve");
        }
    });

});


app.post('/changePassord', (req, res) => {
    const newPassword = req.body.password;
    const actualPassword = req.body.actualPassword;
    const email = req.body.email;

    var find = false;
    var user;
    var position;
    fs.readFile('db.json', 'utf8', function (err, data) {
        const content = JSON.parse(data);
        console.log(content);
        if (content.users.length !== 0) {
            content.users.forEach((element, index) => {
                if (element.email == email && element.password === actualPassword) {
                    find = true
                    position = index
                    user = element
                }
            })
        }

        if (find) {
            user.password = newPassword
            content.users[position] = user
            console.log(content)
            console.log(user)
            const finalData = JSON.stringify(content)
            console.log(finalData)
            fs.writeFile('db.json', finalData, function (err) {
                if (err) throw err;
                res.end(JSON.stringify(user));
            });
        }
        else {
            res.end("Utilisateur non trouve");
        }
    });

});

app.post('/changePassordCode', (req, res) => {
    const newPassword = req.body.password;
    const code = req.body.code;
    const email = req.body.email;



    var find = false;
    var user;
    var position;
    fs.readFile('db.json', 'utf8', function (err, data) {
        const content = JSON.parse(data);
        console.log(content);
        if (content.users.length !== 0) {
            content.users.forEach((element, index) => {
                if (element.email == email && element.resetCode === code) {
                    find = true
                    position = index
                    user = element
                }
            })
        }

        if (find) {
            user.password = newPassword
            content.users[position] = user
            console.log(content)
            console.log(user)
            const finalData = JSON.stringify(content)
            console.log(finalData)
            fs.writeFile('db.json', finalData, function (err) {
                if (err) throw err;
                res.end("OK");
            });
        }
        else {
            res.end("Utilisateur non trouve");
        }
    });

});

app.post('/confirmUser', (req, res) => {
    const code = req.body.code;
    const email = req.body.email;

    var find = false;
    var user;
    var position;
    fs.readFile('db.json', 'utf8', function (err, data) {
        const content = JSON.parse(data);
        console.log(content);
        if (content.users.length !== 0) {
            content.users.forEach((element, index) => {
                if (element.email == email && element.confirmCode === code) {
                    find = true
                    position = index
                    user = element
                }
            })
        }

        if (find) {
            user.isValid = true
            content.users[position] = user
            console.log(content)
            console.log(user)
            const finalData = JSON.stringify(content)
            console.log(finalData)
            fs.writeFile('db.json', finalData, function (err) {
                if (err) throw err;
                res.end("OK");
            });
        }
        else {
            res.end("Utilisateur non trouve");
        }
    });

});

app.listen(PORT, () => {
    console.log(`Server is running on port ${PORT}.`);
});