const db = require("../models");

const type_signalement = db.type_signalement;
const signalement = db.signalement;

const Op = db.Sequelize.Op;

// Create and Save a new type_signalement
exports.create = (req, res) => {
  // Validate request   
  if (!req.body.type) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a type_signalement
  const type_signalement = {
    type: req.body.type
  };

  // Save type_signalement in the database
  db.type_signalement.create(type_signalement)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the type_signalement."
      });
    });
};

// Retrieve all type_signalements from the database.
exports.findAll = (req, res) => {
  console.log("controller")
  console.log(req)
  const type = req.query.type;
  var condition = type ? { type: { [Op.like]: `%${type}%` } } : null;

  type_signalement.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      console.log(err.message)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving type_signalements."
      });
    });
};

// Find a single type_signalement with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  db.type_signalement.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving type_signalement with id=" + id
      });
    });
};

// Update a type_signalement by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  type_signalement.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "type_signalement was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update type_signalement with id=${id}. Maybe type_signalement was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating type_signalement with id=" + id
      });
    });
};

// Delete a type_signalement with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  type_signalement.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "type_signalement was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete type_signalement with id=${id}. Maybe type_signalement was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete type_signalement with id=" + id
      });
    });
};

// Delete all type_signalements from the database.
exports.deleteAll = (req, res) => {
  type_signalement.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} type_signalements were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all type_signalements."
      });
    });
};