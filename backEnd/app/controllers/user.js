const db = require("../models");

const user = db.user;
const signalement = db.signalement;

const Op = db.Sequelize.Op;

// Create and Save a new user
exports.create = (req, res) => {
  // Validate request   
  if (!req.body.type) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a user
  const user = {
    nom: req.body.nom,
    prenom: req.body.prenom,
    mail: req.body.mail,
    mdp: req.body.mdp
  };

  // Save type_signalement in the database
  db.user.create(user)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the type_signalement."
      });
    });
};

// Retrieve all type_signalements from the database.
exports.findAll = (req, res) => {
  console.log("controller")
  console.log(req)
  user.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      console.log(err.message)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving type_signalements."
      });
    });
};

// Find a single type_signalement with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  db.user.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving type_signalement with id=" + id
      });
    });
};

// Update a type_signalement by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  type_signalement.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "User was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update type_signalement with id=${id}. Maybe user was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating user with id=" + id
      });
    });
};

// Delete a user with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  user.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "User was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete user with id=${id}. Maybe user was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete type_signalement with id=" + id
      });
    });
};

// Delete all type_signalements from the database.
exports.deleteAll = (req, res) => {
  type_signalement.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} type_signalements were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all type_signalements."
      });
    });
};