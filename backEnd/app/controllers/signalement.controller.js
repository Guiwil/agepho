const db = require("../models");

const signalement = db.signalement;
const type_signalement = db.type_signalement;

const Op = db.Sequelize.Op;

// Create and Save a new signalement
exports.create = (req, res) => {
  // Validate request   

  // description,
  // latitude,
  // longitude,
  // base64image,
  // type_signalement

  if (!(req.body.description || req.body.latitude || req.body.longitude || req.body.base64image || req.body.typeSignalement )) {
    res.status(400).send({
      message: "Content can not be empty!"
    });
    return;
  }

  // Create a signalement
  const signalement = {
    description: req.body.description,
    latitude: req.body.latitude,
    longitude: req.body.longitude,
    base64image: req.body.base64image,
    typeSignalementId: req.body.typeSignalementId
  };

  // Save signalement in the database
  db.signalement.create(signalement)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while creating the signalement."
      });
    });
};

// Retrieve all signalements from the database.
exports.findAll = (req, res) => {
  console.log("controller")
  console.log(req)
  const type = req.query.type;
  var condition = type ? { type: { [Op.like]: `%${type}%` } } : null;

  signalement.findAll()
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      console.log(err.message)
      res.status(500).send({
        message:
          err.message || "Some error occurred while retrieving signalements."
      });
    });
};

// Find a single signalement with an id
exports.findOne = (req, res) => {
  const id = req.params.id;

  db.signalement.findByPk(id)
    .then(data => {
      res.send(data);
    })
    .catch(err => {
      res.status(500).send({
        message: "Error retrieving signalement with id=" + id
      });
    });
};

// Update a signalement by the id in the request
exports.update = (req, res) => {
  const id = req.params.id;

  signalement.update(req.body, {
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "signalement was updated successfully."
        });
      } else {
        res.send({
          message: `Cannot update signalement with id=${id}. Maybe signalement was not found or req.body is empty!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Error updating signalement with id=" + id
      });
    });
};

// Delete a signalement with the specified id in the request
exports.delete = (req, res) => {
  const id = req.params.id;

  signalement.destroy({
    where: { id: id }
  })
    .then(num => {
      if (num == 1) {
        res.send({
          message: "signalement was deleted successfully!"
        });
      } else {
        res.send({
          message: `Cannot delete signalement with id=${id}. Maybe signalement was not found!`
        });
      }
    })
    .catch(err => {
      res.status(500).send({
        message: "Could not delete signalement with id=" + id
      });
    });
};

// Delete all signalements from the database.
exports.deleteAll = (req, res) => {
  signalement.destroy({
    where: {},
    truncate: false
  })
    .then(nums => {
      res.send({ message: `${nums} signalements were deleted successfully!` });
    })
    .catch(err => {
      res.status(500).send({
        message:
          err.message || "Some error occurred while removing all signalements."
      });
    });
};