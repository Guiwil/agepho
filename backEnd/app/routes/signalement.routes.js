module.exports = app => {
  const signalement = require("../controllers/signalement.controller.js");

  var router = require("express").Router();

  router.post("/", (req, res) => {
    return ({ message: signalement.create(req, res) });   
  });

  // Retrieve all signalement
  router.get("/", (req, res) => {
    return ({ message: signalement.findAll(req, res) });    
  });

  // Retrieve a single signalement with id
  router.get("/:id", (req, res) => {
    return ({ message: signalement.findOne(req, res) });    
});
  // Update a signalement with id
  router.put("/:id", (req, res) => {
    return ({ message: signalement.update(req, res) });    
  });

  // Delete a signalement with id
  router.delete("/:id", (req, res) => {
    return ({ message: signalement.delete(req, res) });    
  });

  // Delete all signalement
  router.delete("/", (req, res) => {
    return ({ message: signalement.deleteAll(req, res) });    
});

  app.use('/api/signalement', router);
};
