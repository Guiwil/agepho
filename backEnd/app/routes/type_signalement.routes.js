module.exports = app => {
  const type_signalement = require("../controllers/type_signalement.controller.js");

  var router = require("express").Router();

  // Retrieve all type_signalement
  router.get("/", (req, res) => {
    return ({ message: type_signalement.findAll(req, res) });    
  });

  // Retrieve a single type_signalement with id
  router.get("/:id", (req, res) => {
    return ({ message: type_signalement.findOne(req, res) });    
});


  app.use('/api/type_signalement', router);

};
