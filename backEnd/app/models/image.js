module.exports = (sequelize, Sequelize) => {
    const image = sequelize.define("image", {
        date: {
            type: Sequelize.STRING
        },
        photo: {
            type: Sequelize.STRING
        }
    });
    return image;
};

