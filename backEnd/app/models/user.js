module.exports = (sequelize, Sequelize) => {
  const user = sequelize.define("user", {
    nom: {
      type: Sequelize.STRING
    },
    prenom: {
      type: Sequelize.STRING
    },
    mail: {
      type: Sequelize.STRING
    },
    password: {
      type: Sequelize.STRING
    },
  });
  return user;
};

