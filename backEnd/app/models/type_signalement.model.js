module.exports = (sequelize, Sequelize) => {
  const type_signalement = sequelize.define("type_signalement", {
    type: {
      type: Sequelize.STRING
    }
  });
  return type_signalement;
};

