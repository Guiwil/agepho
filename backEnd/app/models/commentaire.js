module.exports = (sequelize, Sequelize) => {
    const commentaire = sequelize.define("commentaire", {
        auteur: {
            type: Sequelize.STRING
        },
        contenu: {
            type: Sequelize.STRING
        },
        date: {
            type: Sequelize.DATE
        }
    });
    return commentaire;
};

