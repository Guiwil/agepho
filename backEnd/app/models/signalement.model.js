module.exports = (sequelize, DataTypes) => {
    const signalement = sequelize.define("signalement", {
        description: {
        type: DataTypes.STRING
      },
      latitude: {
        type: DataTypes.STRING
      },
      longitude: {
        type: DataTypes.STRING
      },
      base64image: {
        type: DataTypes.TEXT
      },
    });
  
    return signalement;
  };